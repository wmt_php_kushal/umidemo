import React, { Component } from 'react'
import 'antd/dist/antd.css';
import { Button, Radio, Typography, Card, Avatar, Menu, Dropdown, Icon, Row, Col } from 'antd';
// import Title from 'antd/lib/skeleton/Title';
const { Paragraph,Title } = Typography;
const { Meta } = Card;
export class Top extends Component {
    state = {
        size: 'large',
        str: 'Some text'
    };
    handleMenuClick = (e) => {
        console.log('click', e);
    }
    handleChange = e => {
        this.setState({ size: e.target.value });
    }
    onChange = str => {
        this.setState({ str });
    }
    render() {
        const menu = (
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key='1'>1</Menu.Item>
                <Menu.Item key='2'>2</Menu.Item>
                <Menu.Item key='3'>3</Menu.Item>
            </Menu>
        )
        const { size } = this.state;
        return (
            <div>
                <Row>
                    <Col>
                        <div>
                            <Dropdown overlay={menu}>
                                <Button>
                                    {/* <CaretDownOutlined/> */}
                                    Actions <Icon type="down" />
                                </Button>
                            </Dropdown>
                        </div>
                        <div>
                            <Button type='primary' ghost>Some</Button>
                            <Button type='Default'>Some</Button>
                            <Button type='dashed'>Some</Button>
                            <Button type='danger' ghost>Btn</Button>
                            <br />
                            <Radio.Group value={size} onChange={this.handleChange}>
                                <Radio.Button value='large'>Large</Radio.Button>
                                <Radio.Button value='default' >Default</Radio.Button>
                                <Radio.Button value='small'>Small</Radio.Button>
                                <Button type='danger'></Button>
                            </Radio.Group>
                        </div>
                        <div>
                            <Title><Paragraph editable={{ onChange: this.onChange }} size='large'>{this.state.str}</Paragraph></Title>
                            <Title level={3}><Paragraph copyable>Text to copy</Paragraph></Title>
                            <Paragraph copyable={{ text: this.state.str }}>Replacing the copied text</Paragraph>
                        </div>
                        <div>
                            <Card
                                hoverable
                                style={{ width: 240 }}
                                cover={
                                    <img
                                        alt="example"
                                        src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                                    />
                                }>
                                <Meta avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                    title="Card title"
                                    description="This is the description"
                                />
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>

        )
    }
}

export default Top
